import { SelectionModel } from '@angular/cdk/collections';
import { Component, ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import * as $ from 'jquery';

export interface PeriodicElement {
  recorderName: string;
  migrationStatus: string;
  workStation: string;
  recordStatus: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { recorderName: 'Record01', migrationStatus: 'Migrated', workStation: 'WK00000001', recordStatus: 'ONLINE' },
  { recorderName: 'Record02', migrationStatus: 'Migrated', workStation: 'WK00000002', recordStatus: 'ONLINE' },
  { recorderName: 'Record03', migrationStatus: 'Migrated', workStation: 'WK00000003', recordStatus: 'ONLINE' },
  { recorderName: 'Record04', migrationStatus: 'Migrated', workStation: 'WK00000004', recordStatus: 'ONLINE' },
  { recorderName: 'Record05', migrationStatus: 'Non-Migrated', workStation: 'WK00000005', recordStatus: 'ONLINE' },
  { recorderName: 'Record06', migrationStatus: 'Migrated', workStation: 'WK00000006', recordStatus: 'ONLINE' },
  { recorderName: 'Record07', migrationStatus: 'Migrated', workStation: 'WK00000007', recordStatus: 'OFFLINE' },
  { recorderName: 'Record08', migrationStatus: 'Migrated', workStation: 'WK00000008', recordStatus: 'OFFLINE' },
  { recorderName: 'Record09', migrationStatus: 'Migrated', workStation: 'WK00000009', recordStatus: 'ONLINE' },
  { recorderName: 'Record10', migrationStatus: 'Migrated', workStation: 'WK00000010', recordStatus: 'ONLINE' },
  { recorderName: 'Record11', migrationStatus: 'Migrated', workStation: 'WK00000011', recordStatus: 'OFFLINE' },
  { recorderName: 'Record12', migrationStatus: 'Migrated', workStation: 'WK00000012', recordStatus: 'ONLINE' },
  { recorderName: 'Record13', migrationStatus: 'Migrated', workStation: 'WK00000013', recordStatus: 'OFFLINE' },
];


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  isTablet
  displayedColumns: string[] = ['select', 'recorderName', 'migrationStatus', 'workStation', 'recordStatus', 'Action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    //this.dataSource = new MatTableDataSource(ELEMENT_DATA);
    //this.changeDetector.detectChanges();
    //this.dataSource.sort = this.sort;
    //console.log(this.sort)
    $("#menu-toggle").click(function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.recorderName + 1}`;
  }
}
